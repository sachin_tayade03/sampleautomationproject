package pages.mercury.webpagerepository;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import pages.mercury.webelementrepository.FindFlightElements;

public class FlightDetailsPage implements FindFlightElements{

	public  List<WebElement> captureelement_rdbutton_triptype(WebDriver driver){

		List<WebElement> radios  = driver.findElements(TRIPTYPE);

		return radios;

	}


	public Select  captureelement_drpDown_passengers(WebDriver driver){
		WebElement element = null;

		element = driver.findElement(PASSCOUNT);
		Select selectPassCount = new Select(element);
		return selectPassCount;
	}
	public Select  captureelement_drpDown_departure(WebDriver driver){
		WebElement element = null;

		element = driver.findElement(FROMPORT);
		Select selectfromPort = new Select(element);
		return selectfromPort;
	}
	public Select  captureelement_drpDown_month(WebDriver driver){
		WebElement element = null;

		element = driver.findElement(FROMMONTH);
		Select selectfromMonth = new Select(element);
		return selectfromMonth;
	}
	public Select  captureelement_drpDown_day(WebDriver driver){
		WebElement element = null;

		element = driver.findElement(FROMDAY);
		Select selectfromDay = new Select(element);
		return selectfromDay;
	}

	public Select  captureelement_drpDown_arrival(WebDriver driver){
		WebElement element = null;

		element = driver.findElement(TOPORT);
		Select selecttoPort = new Select(element);
		return selecttoPort;
	}

	public Select  captureelement_drpDown_tomonth(WebDriver driver){
		WebElement element = null;

		element = driver.findElement(TOMONH);
		Select selecttoMonth = new Select(element);
		return selecttoMonth;
	}
	public Select  captureelement_drpDown_today(WebDriver driver){
		WebElement element = null;
		element = driver.findElement(TODAY);
		Select selecttoDay = new Select(element);
		return selecttoDay;
	}
	public List<WebElement> captureelement_rdbutton_serviceclass(WebDriver driver){

		List<WebElement> radios  = driver.findElements(SERVCLASS);
		return radios;

	}

	public Select  captureelement_drpDown_airline(WebDriver driver) {
		WebElement element = null;
		element = driver.findElement(AIRLINE);
		Select selectairline = new Select(element);
		return selectairline;
	}

	public WebElement capturebutton_continue(WebDriver driver){
		WebElement element = null;
		element = driver.findElement(CONTINUEBTTN);

		return element;


	}
}
