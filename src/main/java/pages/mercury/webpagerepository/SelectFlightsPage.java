package pages.mercury.webpagerepository;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import pages.mercury.webelementrepository.SelectFlightElements;

public class SelectFlightsPage implements SelectFlightElements {

	public SelectFlightsPage(){

	}
	
	public  List<WebElement> capture_depart_table(WebDriver driver){


		WebElement table_element = driver.findElement(DEPARTTABLE);
		List<WebElement> tr_collection=table_element.findElements(By.tagName("tr"));

		for (WebElement row : tr_collection.subList(3, 9))	{
			List<WebElement> cells = row.findElements(By.tagName("td"));
			String [] 	cellarray = new String[cells.size()];

			for (int i = 0; i<cells.size();i++){
				cellarray[i] = cells.get(i).getText() + "\t";
			}

		}

		


		return tr_collection;
	}
	
	public  WebElement capturebutton_continue(WebDriver driver){
		WebElement element = null;
		element = driver.findElement(CONTINUEBTTN);

		return element;
		
	}
	
	
}
