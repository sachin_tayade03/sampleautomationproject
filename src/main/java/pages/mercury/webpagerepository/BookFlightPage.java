package pages.mercury.webpagerepository;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import pages.mercury.webelementrepository.BookFlightElements;

public class BookFlightPage implements BookFlightElements {

	public BookFlightPage(){

	}

	public  List<WebElement> capture_reservation_table(WebDriver driver){

		WebElement table_element = driver.findElement(DEPARTTABLE);
		List<WebElement> tr_collection=table_element.findElements(By.tagName("tr"));

		for (WebElement row : tr_collection.subList(3, 12))	{
			List<WebElement> cells = row.findElements(By.tagName("td"));
			String [] 	cellarray = new String[cells.size()];

			for (int i = 0; i<cells.size();i++){
				cellarray[i] = cells.get(i).getText() + "\t";
				//System.out.printf(cells.get(i).getText());
				System.out.printf(cellarray[i]);
			}
			System.out.println();
		}
		return tr_collection;
	}

	public  WebElement captureinput_fname(WebDriver driver) throws InterruptedException{

		//	WebDriverWait wait = new WebDriverWait(driver, 10);

		//	WebElement element = wait.until(ExpectedConditions.elementToBeClickable(FNAME));
		//WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("html/body/div[1]/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[4]/td/table/tbody/tr[2]/td[1]/input")));

		WebElement	element = driver.findElement(FNAME);
		return element;
	}
	public WebElement captureinput_lname(WebDriver driver){
		WebElement element = null;
		element = driver.findElement(LNAME);
		return element;
	}
	public Select  captureelement_drpDown_meal(WebDriver driver){
		WebElement element = null;
		element = driver.findElement(MEAL);
		Select selectmeal = new Select(element);
		return selectmeal;
	}
	public Select  captureelement_drpDown_cardtype(WebDriver driver){
		WebElement cardtype = null;
		cardtype = driver.findElement(CREDITCARD);
		Select selectcard = new Select(cardtype);
		return selectcard;
	}
	public WebElement captureinput_carnumber(WebDriver driver){
		WebElement cardnumber = null;
		cardnumber = driver.findElement(CREDITNUM);
		return cardnumber;
	}
	public Select  captureelement_drpDown_expmonth(WebDriver driver){
		WebElement expmonth = null;
		expmonth = driver.findElement(CCEXPMNTH);
		Select selectexpmonth = new Select(expmonth);
		return selectexpmonth;
	}
	public Select  captureelement_drpDown_expyear(WebDriver driver){
		WebElement expyear = null;
		expyear = driver.findElement(CCEXPYR);
		Select selectexpyear = new Select(expyear);
		return selectexpyear;
	}
	public WebElement captureinput_cc_fname(WebDriver driver){
		WebElement cc_fname = null;
		cc_fname = driver.findElement(CCFNAME);
		return cc_fname;
	}
	public  WebElement captureinput_cc_mname(WebDriver driver){
		WebElement cc_mname = null;
		cc_mname = driver.findElement(CCMNAME);
		return cc_mname;
	}
	public WebElement captureinput_cc_lname(WebDriver driver){
		WebElement cc_lname = null;
		cc_lname = driver.findElement(CCLNAME);
		return cc_lname;
	}
	public WebElement captureinput_cc_address1(WebDriver driver){
		WebElement cc_address = null;
		cc_address = driver.findElement(BILLADDR1);
		return cc_address;
	}
	public  WebElement captureinput_cc_address2(WebDriver driver){
		WebElement cc_lname = null;
		cc_lname = driver.findElement(BILLADDR2);
		return cc_lname;
	}
	public  WebElement captureinput_cc_city(WebDriver driver){
		WebElement city = null;
		city = driver.findElement(BILLCITY);
		return city;
	}
	public  WebElement captureinput_cc_state(WebDriver driver){
		WebElement state = null;
		state = driver.findElement(BILLST);
		return state;
	}
	public  WebElement captureinput_cc_zip(WebDriver driver){
		WebElement billZip = null;
		billZip = driver.findElement(BILLZIP);
		return billZip;
	}
	public  Select  captureelement_drpDown_billcountry(WebDriver driver){
		WebElement billcountry = null;
		billcountry = driver.findElement(BILLCOUNTRY);
		Select selectcountry = new Select(billcountry);
		return selectcountry;
	}
	public  WebElement capturecheckbox_cc_ticketLess(WebDriver driver){
		WebElement ticketLess = null;
		ticketLess = driver.findElement(ADDRCHKBX);
		return ticketLess;
	}
	public  WebElement capturecheckbox_cc_samaddress (WebDriver driver){
		WebElement samaddress = null;
		samaddress = driver.findElement(ADDRCHKBX);
		return samaddress;
	}

	public  WebElement captureinput_cc_delAddress1(WebDriver driver){
		WebElement delAddress1 = null;
		delAddress1 = driver.findElement(DELADDR1);
		return delAddress1;
	}
	public  WebElement captureinput_cc_delAddress2(WebDriver driver){
		WebElement delAddress2 = null;
		delAddress2 = driver.findElement(DELADDR2);
		return delAddress2;
	}
	public  WebElement captureinput_cc_delCity(WebDriver driver){
		WebElement delCity = null;
		delCity = driver.findElement(DELCITY);
		return delCity;
	}
	public  WebElement captureinput_delState(WebDriver driver){
		WebElement delState = null;
		delState = driver.findElement(DELST);
		return delState;
	}
	public  WebElement captureinput_delZip(WebDriver driver){
		WebElement delZip = null;
		delZip = driver.findElement(DELZIP);
		return delZip;
	}
	public  Select  captureelement_drpDown_delCountry(WebDriver driver){
		WebElement delCountry = null;
		delCountry = driver.findElement(DELCOUNTRY);
		Select selectdelCountry = new Select(delCountry);
		return selectdelCountry;
	}
	public  WebElement capturebutton_securepurchase(WebDriver driver){
		WebElement purchasebutton = null;
		purchasebutton = driver.findElement(SECUREPURCHASEBTTN);
		return purchasebutton;
	}


}
