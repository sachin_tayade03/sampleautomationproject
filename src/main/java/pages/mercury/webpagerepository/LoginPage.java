package pages.mercury.webpagerepository;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import pages.mercury.webelementrepository.LoginElements;

public class LoginPage implements LoginElements {

	private static WebElement element = null;

	public WebElement txtBx_LogIn(WebDriver driver){
		element = driver.findElement(USERNAME);
		return element;
	}

	public WebElement txtBx_Password(WebDriver driver){
		element = driver.findElement(PASSWORD);
		return element;
	}
	public static WebElement btn_Login(WebDriver driver){
		element = driver.findElement(LOGINBTTN);
		return element;
	}

}
