package pages.mercury.webpagerepository;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


import pages.mercury.webelementrepository.FlightConfirmationElements;

public class FlightConfirmationPage implements FlightConfirmationElements {

	public List<WebElement> capture_confirmation_table(WebDriver driver){
		
		WebElement table_element = driver.findElement(CONFIRMTABLE);
		List<WebElement> tr_collection=table_element.findElements(By.tagName("tr"));
		
		for (WebElement row : tr_collection.subList(3, 12))	{
			List<WebElement> cells = row.findElements(By.tagName("td"));
			String [] 	cellarray = new String[cells.size()];

			for (int i = 0; i<cells.size();i++){
				cellarray[i] = cells.get(i).getText() + "\t";
				//System.out.printf(cells.get(i).getText());
				System.out.printf(cellarray[i]);
			}
			//System.out.println();
		}
		return tr_collection;
	}

	public  WebElement capture_bkflights_button(WebDriver driver){
		WebElement bkflights_button = null;
		bkflights_button = driver.findElement(BKFLIGHTBTTN);
		return bkflights_button;
	}

	public  WebElement capture_bkhome_button(WebDriver driver){
		WebElement bkhome_button = null;
		bkhome_button = driver.findElement(BKHOMEBTTN);
		return bkhome_button;
	}

	public  WebElement capture_logout_button(WebDriver driver){
		WebElement logout_button = null;
		logout_button = driver.findElement(LOGOUTBBTN);
		return logout_button;
	}
	
	public  WebElement capture_confirmation_number(WebDriver driver){
		WebElement confirmation_number = driver.findElement(CONFIRMNUM);
		System.out.println(confirmation_number.getText());
		
		return confirmation_number;
		
	}
}
