package pages.mercury.webpagerepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import pages.mercury.webelementrepository.HomePageElements;


public class HomePage implements HomePageElements  {

	private static WebElement element = null;


	public WebElement lnk_SignOn(WebDriver driver){

		
		element = driver.findElement(SIGNONLNK);
		Assert.assertTrue((element.isDisplayed()), "Home Page displayed");
		return element;
	}

	public static WebElement btn_Login(WebDriver driver){
		element = driver.findElement(SIGNINBTTN);
		return element;
	}
	
	
	public void verifyHomePage(WebDriver driver){
		Assert.assertTrue((driver.findElement(SIGNONLNK).isDisplayed()), "Home Page displayed");
		
	}
	
}
