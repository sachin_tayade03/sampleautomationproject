package pages.mercury.webelementrepository;
import org.openqa.selenium.By;



public interface FlightConfirmationElements extends CommonElements {
	String CONFIRMTABLE1 = "//img[@src='/images/printit.gif']//following::table";
	By CONFIRMTABLE = By.xpath(String.format(CONFIRMTABLE1));
	String BKFLIGHT= "//img[@src='/images/forms/backtoflights.gif']" ;
	By BKFLIGHTBTTN = By.xpath(String.format(BKFLIGHT));
	String HOME= "//img[@src='/images/forms/home.gif']" ;
	By BKHOMEBTTN = By.xpath(String.format(HOME));
	String LOGOUT= "//img[@src='/images/forms/Logout.gif']" ;
	By LOGOUTBBTN = By.xpath(String.format(LOGOUT));
	By CONFIRMNUM = By.xpath("html/body/div[1]/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr[1]/td[2]/table/tbody/tr[5]/td/table/tbody/tr[1]/td/table/tbody/tr/td[1]/b/font/font/b/font[1]");
	
}
