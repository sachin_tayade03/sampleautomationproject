package pages.mercury.webelementrepository;
import org.openqa.selenium.By;


public interface BookFlightElements extends CommonElements {
	
	By DEPARTTABLE = By.xpath(String.format(DEPARTTABLE1));
	
	By FNAME = By.xpath(String.format(INPUT,"passFirst0"));
	By LNAME = By.xpath(String.format(INPUT, "passLast0"));
	By MEAL = By.xpath(String.format(SELECT, "pass.0.meal"));
	By CREDITCARD = By.xpath(String.format(SELECT, "creditCard"));
	By CREDITNUM = By.xpath(String.format(INPUT, "creditnumber"));
	By CCEXPMNTH = By.xpath(String.format(SELECT, "cc_exp_dt_mn"));
	By CCEXPYR = By.xpath(String.format(SELECT, "c_exp_dt_yr"));
	By CCFNAME = By.xpath(String.format(INPUT, "cc_frst_name")); 
	By CCMNAME = By.xpath(String.format(INPUT, "cc_mid_name")); 
	By CCLNAME = By.xpath(String.format(INPUT, "cc_last_name"));
	By BILLADDR1 = By.xpath(String.format(INPUT,"billAddress1"));
	By BILLADDR2 = By.xpath(String.format(INPUT,"billAddress2"));
	
	By BILLCITY = By.xpath(String.format(INPUT,"billCity"));
	By BILLST = By.xpath(String.format(INPUT,"billState"));
	By BILLZIP = By.xpath(String.format(INPUT,"billZip"));
	By BILLCOUNTRY = By.xpath(String.format(SELECT,"billCountry"));
	
	By ADDRCHKBX = By.xpath(String.format(INPUT,"ticketLess"));
	By DELADDR1 = By.xpath(String.format(INPUT,"delAddress1"));
	By DELADDR2 = By.xpath(String.format(INPUT,"delAddress2"));
	
	By DELCITY = By.xpath(String.format(INPUT,"delCity"));
	By DELST = By.xpath(String.format(INPUT,"delState"));
	By DELZIP = By.xpath(String.format(INPUT,"delZip"));
	By DELCOUNTRY = By.xpath(String.format(SELECT,"delCountry"));
	By SECUREPURCHASEBTTN = By.xpath(String.format(INPUT,"buyFlights"));

}
