package pages.mercury.webelementrepository;

import org.openqa.selenium.By;


public interface SelectFlightElements extends CommonElements {

	By DEPARTTABLE = By.xpath(String.format(SELECTFLIGHTTABLE1,"RETURN"));
	By CONTINUEBTTN = By.xpath(String.format(INPUT,"reserveFlights"));
}
