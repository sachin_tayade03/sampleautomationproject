package pages.mercury.webelementrepository;

import org.openqa.selenium.By;


public interface LoginElements extends CommonElements{	

	By USERNAME = By.xpath(String.format(INPUT,"userName"));
	By PASSWORD = By.xpath(String.format(INPUT,"password"));
	By LOGINBTTN = By.xpath(String.format(INPUT,"login"));
}
