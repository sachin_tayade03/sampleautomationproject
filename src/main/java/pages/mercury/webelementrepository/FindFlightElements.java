package pages.mercury.webelementrepository;

import org.openqa.selenium.By;


public interface FindFlightElements extends CommonElements {
	
	By TRIPTYPE = By.xpath(String.format(RADIO,"tripType"));
	By PASSCOUNT = By.xpath(String.format(SELECT, "passCount"));
	By FROMPORT = By.xpath(String.format(SELECT, "fromPort"));
	By FROMMONTH = By.xpath(String.format(SELECT, "fromMonth"));
	By FROMDAY = By.xpath(String.format(SELECT, "fromDay"));
	By TOPORT = By.xpath(String.format(SELECT, "toPort"));
	By TOMONH = By.xpath(String.format(SELECT, "toMonth"));
	By TODAY = By.xpath(String.format(SELECT, "toDay"));
	By AIRLINE = By.xpath(String.format(SELECT, "airline"));
	By SERVCLASS = By.xpath(String.format(RADIO,"servClass"));
	By CONTINUEBTTN = By.xpath(String.format(INPUT,"findFlights"));
}
