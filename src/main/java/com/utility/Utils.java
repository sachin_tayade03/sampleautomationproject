package com.utility;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

	public class Utils {
	
		public static WebDriver webdriver =null;
	
		/**
		 * @param iTestCaseRow
		 * @return
		 * @throws Exception
		 */
	
		//	@Parameters("Browser")
		public static WebDriver openBrowser(int iTestCaseRow) throws Exception{
			String sBrowserName;
			String sAppUrl;
			sBrowserName = ExcelUtils.getcelldata(iTestCaseRow, ConstantParameters.Col_Browser);
			sAppUrl = ExcelUtils.getcelldata(iTestCaseRow, ConstantParameters.App_URL);
	
			switch (sBrowserName){
			case "Mozzila":
				System.setProperty("webdriver.gecko.driver","C://External Softwares//geckodriver//geckodriver-v0.19.0-win64//geckodriver.exe");
				FirefoxOptions options = new FirefoxOptions();
				//options.setBinary("C://Program Files//Mozilla Firefox//firefox.exe");
				options.setBinary("C://Program Files//Firefox Developer Edition//firefox.exe");
				webdriver = new FirefoxDriver(options);
				webdriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
				webdriver.get(sAppUrl);
				Log.info("Web application launched");
				webdriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				break;
			case "Chrome":
				System.setProperty("webdriver.chrome.driver", "C://External Softwares//chromedriver//chromedriver.exe");
	
				ChromeOptions chromeoptions = new ChromeOptions();
				chromeoptions.setExperimentalOption("useAutomationExtension", false);
				chromeoptions.addArguments("start-maximized");
	
	
				webdriver = new ChromeDriver(chromeoptions);
				webdriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				webdriver.get(sAppUrl);
				Log.info("Web application launched");
				webdriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				break;
			case "IE":
				System.setProperty("webdriver.ie.driver", "C://External Softwares//IEDriverServer_Win32_2.41.0//IEDriverServer.exe");
				webdriver = new InternetExplorerDriver();
				webdriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				webdriver.get(sAppUrl);
				Log.info("Web application launched");
				webdriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				break;
			default:
				System.out.println("Not a valid browser");
				break;
			}
	
	
			return webdriver;
	
		}
	
		/**
		 * This Method converts the toString value returned by Test Class and trims it to Test Name 
		 * @param sTestName - parameter for tostring value sent by Test Class
		 * @return
		 */
		public static String gettestname(String sTestName){
	
			String sValue;
			int  iPosition;
			iPosition = sTestName.indexOf("@");
			sValue = sTestName.substring(sTestName.lastIndexOf(".")+1, iPosition);
			return sValue;
	
		}
		
		public static WebDriver openmultibrowser(int iTestCaseRow, String sBrowser){
			WebDriver webdriver;
			String sAppUrl = ExcelUtils.getcelldata(iTestCaseRow, ConstantParameters.App_URL);
		System.out.println();
			if (sBrowser.equalsIgnoreCase("firefox"))
		        {
				 System.setProperty("webdriver.gecko.driver","C://External Softwares//geckodriver//geckodriver-v0.19.0-win64//geckodriver.exe");
					FirefoxOptions options = new FirefoxOptions();
					//options.setBinary("C://Program Files//Mozilla Firefox//firefox.exe");
					options.setBinary("C://Program Files//Firefox Developer Edition//firefox.exe");
					webdriver = new FirefoxDriver(options);
					webdriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
					webdriver.get(sAppUrl);
					Log.info("Web application launched");
					webdriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		        } else if (sBrowser.equalsIgnoreCase("Chrome"))
		        {
		        	System.setProperty("webdriver.chrome.driver", "C://External Softwares//chromedriver//Latest Chrome Driver//chromedriver.exe");
		        	
					ChromeOptions chromeoptions = new ChromeOptions();
					chromeoptions.setExperimentalOption("useAutomationExtension", false);
					chromeoptions.addArguments("start-maximized");
		
		
					webdriver = new ChromeDriver(chromeoptions);
					webdriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
					webdriver.get(sAppUrl);
					Log.info("Web application launched");
					webdriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		        }
		        else if (sBrowser.equalsIgnoreCase("ie"))
		        {
		        System.setProperty("webdriver.ie.driver", "D://Selenium/IEDriverServer.exe");
		        webdriver = new InternetExplorerDriver();
		        }
		        else
		        {
		       throw new IllegalArgumentException("The Browser Type is Undefined");
		        }
			return webdriver;
		       
		      }
			
		}

