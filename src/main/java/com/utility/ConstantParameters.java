package com.utility;

public class ConstantParameters {
	public static final String path_TestData = "C://Users//gmqtaya//workspace//sampleframework//src//main//resources//datatables//";
	public static final String file_TestData= "mercuryTestData.xlsx";
	public static final String log_filepath = "C://Users//gmqtaya//workspace//sampleframework//src//main//resources//log4j.xml";
	public static final int Col_TestCaseName =0;
	public static final int App_URL =1;
	public static final int Col_UserName =2;
	public static final int Col_Password =3;
	public static final int Col_Browser =4;
}
