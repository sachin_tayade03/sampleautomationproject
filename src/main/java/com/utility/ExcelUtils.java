/**====================================================================
 *@author gmqtaya
 *This class has methods to read data from Excel files
 *Uses Apache Poi 3.16
 *add Maven dependency to use Apache Poi
==================================================================== */
package com.utility;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.SpreadsheetVersion;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.utility.ConstantParameters;
import com.utility.Log;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFCell;

public class ExcelUtils {


	private static Workbook ExlWBook;
	private static Sheet ExlWSheet;
	private static Cell ExlCell;
	private static Row ExlRow;

	/**
	 * Method to read the data sheet with information for current test case
	 * @param sPath
	 * @param sFileName
	 * @param sSheetName
	 * @throws Exception
	 */
	public static void setexcelfile(String sPath, String sFileName, 
			String sSheetName) throws Exception{

		try {
			FileInputStream ExcelFile = new FileInputStream(sPath);
			String sFileExtension = sFileName.substring(sFileName.indexOf("."));

			if (sFileExtension.equals(".xls")){
				ExlWBook = new HSSFWorkbook(ExcelFile);
				ExlWSheet = (HSSFSheet)ExlWBook.getSheet(sSheetName);
			}
			else {
				ExlWBook = new XSSFWorkbook(ExcelFile);
				ExlWSheet = (XSSFSheet)ExlWBook.getSheet(sSheetName);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw(e);
		}


	}

	/**
	 * Method to get /read the cell content when row and column number are provided
	 * @param iRowNum
	 * @param iColNum
	 * @return 
	 */
	
	public static String getcelldata(int iRowNum, int iColNum){

		String sCellData = null;

		ExlCell = ExlWSheet.getRow(iRowNum).getCell(iColNum);
		CellType CelType = ExlCell.getCellTypeEnum();
		String sCellType = CelType.name();

		if (sCellType.equalsIgnoreCase("STRING")){
			sCellData = ExlCell.getStringCellValue();
		}

		else if (sCellType.equalsIgnoreCase("BLANK")){
			sCellData = "";
		}

		else if (sCellType.equalsIgnoreCase("NUMERIC")){

			double dCellData;

			dCellData = ExlCell.getNumericCellValue();

			if(dCellData == (long) dCellData){
				sCellData =  String.format("%d",(long)dCellData);
			}
			else{
				sCellData = String.format("%s",dCellData);
			}	
		}
		else if (sCellType.equalsIgnoreCase("FORMULA")){
			sCellData = ExlCell.getCellFormula();
		}

		else {

			sCellData = "";
		}

		return sCellData;
	}

	/**
	 * Method to set or write status in a row for a test case
	 * @param rowNum
	 * @param colNum
	 * @param sStatus
	 * @throws Exception
	 */
	public static void setcellvalue(int rowNum, int colNum,String sStatus) throws Exception{
		try {
			ExlCell = ExlWSheet.getRow(rowNum).getCell(colNum, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL);

			if (ExlCell == null) {
				ExlCell = ExlRow.createCell(colNum);
				ExlCell.setCellValue(sStatus);
			} else {
				ExlCell.setCellValue(sStatus);
			}

			FileOutputStream FileOut = new FileOutputStream(ConstantParameters.path_TestData + ConstantParameters.file_TestData);

			ExlWBook.write(FileOut);


			FileOut.flush();

			FileOut.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw e;
		}
	}
	
	/**
	 * Method to get the current Test Case row number from Datasheet
	 * @param sTestCaseName
	 * @param iColNum
	 * @return iTestRow
	 */
	public static int gettestrownumber(String sTestCaseName, int iColNum){
		
		int iRowCount = ExlWSheet.getLastRowNum();
		int iTestRow;
		for (iTestRow = 0; iTestRow<iRowCount; iTestRow++ ){
			if (getcelldata(iTestRow, iColNum).equalsIgnoreCase(sTestCaseName)){
				break;
			}
			
		}
		return iTestRow;		
	}
	
	
	public String[][]  readallexcel(){


		String[][] xldata =null;
		int xlRowCount = ExlWSheet.getLastRowNum()+1;
		int xlCellCount = ExlWSheet.getRow(0).getLastCellNum();

		xldata = new String[xlRowCount][xlCellCount];

		for (int i = 0; i < xlRowCount; i++){

			Row row = ExlWSheet.getRow(i);
		
			for (int j = 0; j < xlCellCount; j++){
				
				Cell datacell = row.getCell(j);
				CellType type = datacell.getCellTypeEnum();
				String celltype = type.name();		
		
				if (celltype.equalsIgnoreCase("STRING")){
					
					xldata [i][j] = datacell.getStringCellValue();
					
				}
				else if (celltype.equalsIgnoreCase("BLANK")){
					xldata [i][j] = "";
				}

				else if (celltype.equalsIgnoreCase("NUMERIC")){
					double CellData1 = datacell.getNumericCellValue();
					xldata [i][j] = Double.toString(CellData1);
				}

				else if (celltype.equalsIgnoreCase("FORMULA")){
					xldata [i][j] = datacell.getCellFormula();	
				}
				
				else 
				{
					xldata [i][j] = "";
				}
		
			}
		}

		return xldata;

	}

	
	
	public static int getrownum( String sTestCaseName, int colNum) throws Exception{

		try {

			int iRowCount = ExlWSheet.getLastRowNum();
			int iCount;
			for (  iCount=0; iCount<=iRowCount; iCount++){
			if (getcelldata(iCount,colNum).equalsIgnoreCase(sTestCaseName)){
					break;
				}

			}
			return iCount;
		} catch (Exception e) {
			Log.error("Class ExcelUtil | Method getRowContains | Exception desc : " + e.getMessage());
			throw(e);
		}
	}
	
	public static int getColNum (String sColumnName) throws Exception{
		
		//	String sTestCaseName = sTestName;
			int iColNum =0;
			
			
			try {
				int iRowNum	;		
		
				int xlCellCount = ExlWSheet.getRow(0).getLastCellNum();
				
				for (int iColCount=0;iColCount< xlCellCount; iColCount++ ){
					String sColNm = getcelldata(ConstantParameters.Col_TestCaseName, iColCount);
					if (sColNm.equalsIgnoreCase(sColumnName)){
						
						iColNum = iColCount;
						break;
					}
					
				}
				
				return iColNum;
			} 
			
			
			
		
			catch (Exception e) {
				// TODO Auto-generated catch block
				Log.error("Class ExcelUtil | Method getRowContains | Exception desc : " + e.getMessage());
				throw(e);
			}
			
			
			
		}
		
	
	public static String getCellCotent(String sColNm, String sTestName, int iColNum) throws Exception{
		String sCellData = null;
		int iRowNum = getrownum(sTestName,iColNum);
		int iColNumber= getColNum(sColNm);
		sCellData = getcelldata(iRowNum, iColNumber );
		return sCellData;
		
		
	}
	
}
