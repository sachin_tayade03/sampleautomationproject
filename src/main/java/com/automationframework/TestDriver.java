package com.automationframework;
import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.WebDriver;
import com.utility.ConstantParameters;
import com.utility.ExcelUtils;
import com.utility.Log;
import com.utility.Utils;


public class TestDriver {

	public WebDriver wbDriver;
	public String sTestCaseName;
	private int iTestCaseRow;
	String sBrowser;

	public WebDriver setuptest(String sTestName, String sBrowserName) throws Exception{
		
		
		DOMConfigurator.configure(ConstantParameters.log_filepath);
		sTestCaseName = Utils.gettestname(sTestName.toString());
		Log.startTestCase(sTestCaseName);
		//try {
			ExcelUtils.setexcelfile(ConstantParameters.path_TestData + ConstantParameters.file_TestData,  ConstantParameters.file_TestData, "Sheet1");
	//	} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		//}
		iTestCaseRow = ExcelUtils.gettestrownumber(sTestCaseName, ConstantParameters.Col_TestCaseName);
		
		wbDriver = Utils.openmultibrowser(iTestCaseRow,sBrowserName); 
		//wbDriver = Utils.openBrowser(iTestCaseRow);
		return wbDriver;
	}

	
	
	public String[][] getexceldata() {
		ExcelUtils EXLUIL = new ExcelUtils();
		String [][]getexldata = EXLUIL.readallexcel();
		
	/*	for (int i = 0; i< getexldata.length; i++){
			for ( intj=0; j<getexldata[i].length(); j++){
				
			}
		}*/
		
		return getexldata;
	}



	
	
}
