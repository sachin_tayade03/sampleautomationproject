package com.businessfunction;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;


import com.businessfunction.ReusableLiberary;
import pages.mercury.webpagerepository.BookFlightPage;
import pages.mercury.webpagerepository.FlightConfirmationPage;
import pages.mercury.webpagerepository.HomePage;
import pages.mercury.webpagerepository.FlightDetailsPage;
import pages.mercury.webpagerepository.LoginPage;
import pages.mercury.webpagerepository.SelectFlightsPage;

import com.utility.ExcelUtils;

public class BusinessComponents {
	ReusableLiberary ReusableLiberaryObj = new ReusableLiberary();


	public  void login(WebDriver driver,String testname) throws Exception{

		HomePage HomePageObj = new HomePage();
		LoginPage LogInPageObj = new LoginPage();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		ReusableLiberaryObj.element_click(HomePageObj.lnk_SignOn(driver), "Sign ON");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		String sUserName = ExcelUtils.getCellCotent("param1", testname, 0);
		String sPassword = ExcelUtils.getCellCotent("param2", testname, 0);
		//String sBttNm = "Login Button";
	
		driver.switchTo().defaultContent();

		ReusableLiberaryObj.element_enterdata(LogInPageObj.txtBx_LogIn(driver), sUserName);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		//	LogIn_Page.txtBx_Password(driver).sendKeys(sPassword);
		ReusableLiberaryObj.element_enterdata(LogInPageObj.txtBx_Password(driver), sPassword);
/*		System.out.println("Data for ID password entered");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		ReusableLiberaryObj.element_click(LoginPage.btn_Login(driver), "login");*/
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		LoginPage.btn_Login(driver).click();
		//System.out.println("Button Clicked");


	}



	public   void searchflights(WebDriver driver, String testname) throws Exception{

		String sTripType = ExcelUtils.getCellCotent("param4", testname, 0);
		String sPassCount = ExcelUtils.getCellCotent("param5", testname, 0);
		String sDepartureCity = ExcelUtils.getCellCotent("param6", testname, 0);
		String sFmonth = ExcelUtils.getCellCotent("param7", testname, 0);
		String sFday = ExcelUtils.getCellCotent("param8", testname, 0);
		String sArrivalCity = ExcelUtils.getCellCotent("param9", testname, 0);
		String sToMonth = ExcelUtils.getCellCotent("param10", testname, 0);
		String sToDay = ExcelUtils.getCellCotent("param11", testname, 0);
		String sServClass = ExcelUtils.getCellCotent("param12", testname, 0);
		String sAirLine = ExcelUtils.getCellCotent("param13", testname, 0);



		FlightDetailsPage 		FlightDetailsObj = new FlightDetailsPage();

		ReusableLiberaryObj.element_select_radiobutton(FlightDetailsObj.captureelement_rdbutton_triptype(driver), sTripType);	
		ReusableLiberaryObj.element_select_dropdown(FlightDetailsObj.captureelement_drpDown_passengers(driver), sPassCount);
		ReusableLiberaryObj.element_select_dropdown(FlightDetailsObj.captureelement_drpDown_departure(driver), sDepartureCity);
		ReusableLiberaryObj.element_select_dropdown(FlightDetailsObj.captureelement_drpDown_month(driver), sFmonth);
		ReusableLiberaryObj.element_select_dropdown(FlightDetailsObj.captureelement_drpDown_day(driver), sFday);
		ReusableLiberaryObj.element_select_dropdown(FlightDetailsObj.captureelement_drpDown_departure(driver), sArrivalCity);
		ReusableLiberaryObj.element_select_dropdown(FlightDetailsObj.captureelement_drpDown_tomonth(driver), sToMonth);
		ReusableLiberaryObj.element_select_dropdown(FlightDetailsObj.captureelement_drpDown_today(driver), sToDay);
		ReusableLiberaryObj.element_select_radiobutton(FlightDetailsObj.captureelement_rdbutton_serviceclass(driver),sServClass);
		ReusableLiberaryObj.element_select_dropdown_optionOnly(FlightDetailsObj.captureelement_drpDown_airline(driver), sAirLine);
		ReusableLiberaryObj.element_click(FlightDetailsObj.capturebutton_continue(driver), "Continue");

	}

	public  void selectflight(WebDriver driver){
		SelectFlightsPage SelectFlightPage = new SelectFlightsPage();
		SelectFlightPage.capture_depart_table(driver);
		//SelectFlightPage.capturebutton_continue(driver).click();
		ReusableLiberaryObj.element_click(SelectFlightPage.capturebutton_continue(driver), "Select Flight");
		//	ReusableLiberaryObj.element_select_radiobutton(SelectFlightPage.capturebutton_continue(driver), "Select Flight");
	}


	public void bookflight(WebDriver driver, String testname) throws Exception{

		//	businessAction.ReusableLiberary br = new businessAction.ReusableLiberary();
		BookFlightPage BookFlight_PageObj = new BookFlightPage();


		String sFname = ExcelUtils.getCellCotent("param14", testname, 0);
		String sLname = ExcelUtils.getCellCotent("param15", testname, 0);
		String sMealOption = ExcelUtils.getCellCotent("param16", testname, 0);
		String sCardType = ExcelUtils.getCellCotent("param17", testname, 0);
		String sCardNum = ExcelUtils.getCellCotent("param18", testname, 0);



		String sCardExpMonth = ExcelUtils.getCellCotent("param19", testname, 0);

		int iCardExpMonth = Integer.parseInt(sCardExpMonth);


		String sCardExpYear = ExcelUtils.getCellCotent("param20", testname, 0);
		String sCardFname = ExcelUtils.getCellCotent("param21", testname, 0);
		String sCardLname = ExcelUtils.getCellCotent("param22", testname, 0);
		String sCardAdd1 = ExcelUtils.getCellCotent("param23", testname, 0);
		String sCardAdd2 = ExcelUtils.getCellCotent("param24", testname, 0);
		String sCardCity = ExcelUtils.getCellCotent("param25", testname, 0);
		String sCardState = ExcelUtils.getCellCotent("param26", testname, 0);
		String sCardZip = ExcelUtils.getCellCotent("param27", testname, 0);




		ReusableLiberaryObj.element_enterdata(BookFlight_PageObj.captureinput_fname(driver), sFname);
		ReusableLiberaryObj.element_enterdata(BookFlight_PageObj.captureinput_lname(driver),sLname);
		ReusableLiberaryObj.element_select_dropdown(BookFlight_PageObj.captureelement_drpDown_meal(driver),sMealOption);
		ReusableLiberaryObj.element_select_dropdown(BookFlight_PageObj.captureelement_drpDown_cardtype(driver),sCardType);
		ReusableLiberaryObj.element_enterdata(BookFlight_PageObj.captureinput_carnumber(driver),sCardNum);

		// This has to be updated
		ReusableLiberaryObj.element_select_dropdownbyindex((BookFlight_PageObj.captureelement_drpDown_expmonth(driver)),iCardExpMonth);

		ReusableLiberaryObj.element_select_dropdown(BookFlight_PageObj.captureelement_drpDown_expyear(driver),sCardExpYear);
		ReusableLiberaryObj.element_enterdata(BookFlight_PageObj.captureinput_cc_fname(driver),sCardFname);
		ReusableLiberaryObj.element_enterdata(BookFlight_PageObj.captureinput_cc_lname(driver),sCardLname);
		ReusableLiberaryObj.element_enterdata(BookFlight_PageObj.captureinput_cc_address1(driver),sCardAdd1);
		ReusableLiberaryObj.element_enterdata(BookFlight_PageObj.captureinput_cc_address2(driver),sCardAdd2);
		ReusableLiberaryObj.element_enterdata(BookFlight_PageObj.captureinput_cc_city(driver),sCardCity);
		ReusableLiberaryObj.element_enterdata(BookFlight_PageObj.captureinput_cc_state(driver),sCardState);
		ReusableLiberaryObj.element_enterdata(BookFlight_PageObj.captureinput_cc_zip(driver),sCardZip);
		ReusableLiberaryObj.element_chkbox_select(BookFlight_PageObj.capturecheckbox_cc_samaddress(driver), "Same Address");
		ReusableLiberaryObj.element_click(BookFlight_PageObj.capturebutton_securepurchase(driver), "Secure Purchase");
	}

	public  void confirmflight(WebDriver driver){

		FlightConfirmationPage FlightConfirmationPage = new FlightConfirmationPage();
		FlightConfirmationPage.capture_confirmation_number(driver);
		FlightConfirmationPage.capture_bkhome_button(driver).click();

	}

}
