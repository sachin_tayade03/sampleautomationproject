package com.businessfunction;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.utility.Log;

public class ReusableLiberary {
	public  void element_click(WebElement element, String sActionElement){


		Assert.assertTrue((element.isDisplayed()),  sActionElement + " - Button displayed." );
		Log.info(element + " Element Displayed " + (element.isDisplayed()));
		
		element.click();
	}
	public  void element_select_radiobutton(List<WebElement> rbutton, String input){



		for (WebElement element : rbutton) {


			if (element.getAttribute("value").equalsIgnoreCase(input)) {

				Assert.assertTrue((element.isDisplayed()),  input + " - Option displayed." );

			}
			element.click();



		}
	}
	public  void element_select_dropdown(Select dropdown, String input){

		dropdown.selectByValue(input);


	}


	public  void element_select_dropdownbyindex(Select dropdown, int input){

		dropdown.selectByIndex(input);

	}


	/**
	 * @param dropdown page element for airline drop down
	 * @param input String for value of airline selection
	 * This method deals with airline drop down which has no value hence normal drop down function does'nt work 
	 */
	public  void element_select_dropdown_optionOnly(Select dropdown, String input){
		List<WebElement> options  = dropdown.getOptions();
		for (WebElement element : options) {

			if (element.getAttribute("value").equalsIgnoreCase(input)) {

				element.click();


			}

		}

	}

	public void element_chkbox_select(WebElement element, String sChkBoxElement){

		element.click();

	}

	public  void element_enterdata(WebElement element, String input){
		element.sendKeys(input);


	}

}
