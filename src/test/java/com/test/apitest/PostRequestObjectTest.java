package com.test.apitest;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static com.jayway.restassured.RestAssured.*;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.test.apitest.Posts;



public class PostRequestObjectTest {

	@BeforeClass
	public void setbaseuri(){
		RestAssured.baseURI = "http://localhost:3000";
	}
	
	@Test
	  public void sendPostObject () {
	  //  int iCount = 0;
	    
	    for(int i=5; i<10; i++ ){
		
	    Posts post = new Posts();
	    post.setId (i);
	    post.setTitle ("Hello World");
	    post.setAuthor ("TestObjWriter123");
		    
	    given().body (post)
	    .when ()
	    .contentType (ContentType.JSON)
	    .post ("/posts");
	    
	    }
	  }
}
