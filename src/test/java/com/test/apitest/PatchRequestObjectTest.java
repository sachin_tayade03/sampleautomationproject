package com.test.apitest;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static com.jayway.restassured.RestAssured.*;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.test.apitest.Posts;



@SuppressWarnings("unused")
public class PatchRequestObjectTest {

	@BeforeClass
	public void setbaseuri(){
		RestAssured.baseURI = "http://localhost:3000";
	}
	
	@Test
	  public void updatePatchObject () {
	   int iCount = 3;
	    
	  
		
	    Posts post = new Posts();
	    post.setId (iCount);
	    post.setTitle ("Hello WstPatch World");
	    post.setAuthor ("TestObjWriter123");
		    
	    given().body (post)
	    .when ()
	    .contentType (ContentType.JSON)
	    .patch ("/posts/6");
	    
	
	  }
}
