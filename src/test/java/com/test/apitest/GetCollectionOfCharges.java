package com.test.apitest;
import static org.testng.Assert.assertEquals;
import java.util.Locale;
import org.testng.annotations.Test;
import com.test.libraries.StripeUtils;
import com.test.maps.CollectionOfCharges;
public class GetCollectionOfCharges {
	
	@Test
	
	public void test_printCollectionOfCharges(){

		String sExpectedObjectType = "list";  
		String sExpectedUrl = "/v1/charges";  
		Integer numberOfChargesRequested = 2; 
		
		StripeUtils stripe = new StripeUtils(Locale.US);
		CollectionOfCharges cCharges = stripe.getListOfCharges(numberOfChargesRequested);
	
		String sActualObject = cCharges.getObject();
		stripe.checkMatchingValues("Verify that the Returned Collection Object types match", sActualObject, sExpectedObjectType);
		assertEquals(cCharges.getObject(), sExpectedObjectType); 
		String sActualURL = cCharges.getUrl();
		stripe.checkMatchingValues("Verify that the Returned URLs match", sActualURL, sExpectedUrl);
	
	}

}
