package com.test.apitest;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static com.jayway.restassured.RestAssured.*;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.test.apitest.Posts;



@SuppressWarnings("unused")
public class PutRequestObjectTest {

	@BeforeClass
	public void setbaseuri(){
		RestAssured.baseURI = "http://localhost:3000";
	}
	
	@Test
	  public void sendPutObject () {
	   int iCount = 11;
	    
	  
		
	    Posts post = new Posts();
	    post.setId (iCount);
	    post.setTitle ("Hello Wst World");
	    post.setAuthor ("TestObjWriter2");
		    
	    given().body (post)
	    .when ()
	    .contentType (ContentType.JSON)
	    .put ("/posts/9");
	    
	
	  }
}
