package com.test.apitest;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static com.jayway.restassured.RestAssured.*;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.path.xml.XmlPath;
import com.jayway.restassured.response.Response;

public class GetXMLResponseTest {

	@BeforeClass
	  public void setBaseUri () {

	    RestAssured.baseURI = "https://maps.googleapis.com";
	  }
	
	
	@Test
	  public void testStatusCodeXMLGoogleApi () {
	    
	    Response res = 
	    given ()

	    .param ("query", "Fry's near 85027")
	    
	    //AIzaSyBrhdZP1wWpMXVEvzpY4-3W-FKieCYhVXg -- sample key
	    // Personal actual key - AIzaSyAhLTcgrAcw-9VWgWsez77jV4oao0y7_jM
	    .param ("key", "AIzaSyAhLTcgrAcw-9VWgWsez77jV4oao0y7_jM")
	    .when()
	    .get ("/maps/api/place/textsearch/xml")
	    .then()
		.contentType(ContentType.XML)
		.extract()
		.response();
	    // xmlPath string traver the xml and converts the response from xml to string for validtion
	    
		String xmlPath = res.path ("placesearchresponse.result[0].formatted_address");
//	    Print the results / response
	    System.out.println (res.asString ());

//	    Validate the status code
	   // Assert.assertEquals (res.statusCode (), 200);
//	    Validate the exact location by providing the JSON Path expression to access the results / response
	    Assert.assertEquals (xmlPath, "Union Plaza Shopping Center, 18420 N 19th Ave, Phoenix, AZ 85023, United States");
	    System.out.println(xmlPath);

	    
	  }
}
