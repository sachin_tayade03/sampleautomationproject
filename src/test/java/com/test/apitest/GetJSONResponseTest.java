package com.test.apitest;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static com.jayway.restassured.RestAssured.*;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;


public class GetJSONResponseTest {

	@BeforeClass
	  public void setBaseUri () {
	    RestAssured.baseURI = "https://maps.googleapis.com";
	  }
	
	@Test
	  public void testJSONResponse () {
	    
	    Response res = 
	    given ()

	    .param ("query", "Fry's near 85027")
	    
	    //AIzaSyBrhdZP1wWpMXVEvzpY4-3W-FKieCYhVXg -- sample key
	    // Personal actual key - AIzaSyAhLTcgrAcw-9VWgWsez77jV4oao0y7_jM
	    .param ("key", "AIzaSyAhLTcgrAcw-9VWgWsez77jV4oao0y7_jM")
	    .when()
	    .get ("/maps/api/place/textsearch/json")
	    .then()
		.contentType(ContentType.JSON)
		.assertThat ().statusCode (200)
		.extract()
		.response();
	    System.out.println (res.asString ());
	 //   .path ("results[0].formatted_address");
	    String sPath = res.path ("results[0].formatted_address");
	    
//	    Validate the status code
	    Assert.assertEquals (res.statusCode (), 200);
//	    Validate the exact location by providing the JSON Path expression to access the results / response
	    Assert.assertEquals (sPath, "Union Plaza Shopping Center, 18420 N 19th Ave, Phoenix, AZ 85023, United States");
//	    Print the results / response
	    System.out.println (res.asString ());
	   
	  }
	@Test
	public void testStatusCodeRestAssured () {

	given ().param ("query", "restaurants in mumbai")
	        .param ("key", "AIzaSyAhLTcgrAcw-9VWgWsez77jV4oao0y7_jM")

	        .when()
	        .get ("/maps/api/place/textsearch/json")
	        .then ()
	        .assertThat ().statusCode (200);
			System.out.println ( "Validation Passed for testStatusCodeRestAssured ");

	}

	
}
