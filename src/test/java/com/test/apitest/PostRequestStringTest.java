package com.test.apitest;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static com.jayway.restassured.RestAssured.*;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;


public class PostRequestStringTest {

	@BeforeClass
	public void setbaseuri(){
		RestAssured.baseURI = "http://localhost:3000";
	}
	
	@Test
	  public void postString () {
	    
	    given().body ("{\"id\":\"3\","
	    +"\"title\":\"Hello JSON\","
	    +"\"author\":\"TestWriter\"}")
	    .when ()
	    .contentType (ContentType.JSON)
	    .post ("/posts");
	      
	  }
}
