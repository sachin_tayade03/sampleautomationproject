package com.test.apitest;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static com.jayway.restassured.RestAssured.*;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;

public class RequestSpecificationTest {

	  RequestSpecification rspec;
	  RequestSpecBuilder build;
	
	@BeforeClass
	
	public void requestSpec(){
		
		build = new RequestSpecBuilder();
		build.setBaseUri("https://maps.googleapis.com");
		build.setBasePath("maps/api/place/textsearch/json");
		build.addParam("query", "restaurants near 85383");
		build.addParam("key", "AIzaSyBrhdZP1wWpMXVEvzpY4-3W-FKieCYhVXg");
	
		rspec = build.build();
	}
	
	@Test
	
	public void testRequestSpec(){
		Response res = 	given()
		.spec(rspec)
		.when()
		.get("")
		.then()
		.contentType(ContentType.JSON)
		.assertThat ().statusCode (200)
		.extract().response();
		System.out.println (res.asString ());
	}
	
	
}
