package com.test.apitest;
import com.test.libraries.StripeUtils;  
import org.testng.annotations.Test;  
import java.util.Locale;  


public class StripeTest {

	@Test  
	   public void test_validCountry() {  
	     StripeUtils stripe = new StripeUtils(Locale.US);  
	     System.out.println(stripe.toString());
	     
	     // This will always pass.  
	   }  
	   @Test  
	   public void test() {  
	     StripeUtils stripe = new StripeUtils(Locale.GERMANY);  
	     System.out.println(stripe.toString());
	     
	     // This will always fail: ERROR: Stripe is used only for US. Country entered: de_DE   
	   }  
	
}
