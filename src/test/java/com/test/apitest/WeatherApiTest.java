package com.test.apitest;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.automationframework.WeatherApiResponse;

import org.apache.http.client.ClientProtocolException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class WeatherApiTest {

	private WebDriver driver;
	private String baseUrl;
	@BeforeTest
	public void setUp() throws Exception {
//		System.setProperty("webdriver.gecko.driver","C://External Softwares//geckodriver//geckodriver.exe");
//		  driver = new FirefoxDriver();
		  
		  System.setProperty("webdriver.chrome.driver", "C://External Softwares//chromedriver/chromedriver.exe");
		  driver = new ChromeDriver();
		  
		    baseUrl = "http://openweathermap.org/current";
		    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		  }

	@Test
	public void test() throws ClientProtocolException, IOException {
		driver.get(baseUrl);
		driver.navigate().to("http://api.openweathermap.org/data/2.5/weather?q=London");	
	 WebElement webElement=driver.findElement(By.tagName("pre"));
	 WeatherApiResponse weatherApiResponse=new WeatherApiResponse();
	 String ExpectedString=weatherApiResponse.GetResponse();
	 Assert.assertTrue(webElement.getText().equals(ExpectedString));
	}
	
	@AfterTest
	public void tearDown() throws Exception {
		 driver.close();
		 driver.quit();
	}
}
