package com.test.integration_test;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

import com.utility.Log;
import com.utility.Utils;

public class NewTest1 {
	private WebDriver driver;
	 
    @BeforeClass
    public void beforeClass() {
     	System.setProperty("webdriver.gecko.driver","C://External Softwares//geckodriver//geckodriver-v0.19.0-win64//geckodriver.exe");
    	   FirefoxOptions options = new FirefoxOptions();
    	    options.setBinary("C://Program Files//Mozilla Firefox//firefox.exe");
      	    FirefoxDriver driver = new FirefoxDriver(options);
    	    driver.get("http://www.google.com");

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
   }
 
    @AfterClass
    public void afterClass() {
     // driver.close();
    }
 
	
  @Test
  public void verifySearchButton() {
	  
      driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

      driver.get("http://www.google.com");

      String search_text = "Google Search";
      WebElement search_button = driver.findElement(By.name("btnK"));

      String text = search_button.getAttribute("value");
      
      String oTest = this.toString();
      System.out.println(oTest);
      
      String test = "Test_class.1647@ndkdhff";
      
     System.out.println(Utils.gettestname(oTest));      
      
      Assert.assertEquals(text, search_text, "Text not found!");
  }


}
