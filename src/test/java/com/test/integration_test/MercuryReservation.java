package com.test.integration_test;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.automationframework.TestDriver;
import com.businessfunction.BusinessComponents;




public class MercuryReservation {
	
	public WebDriver driver;
	public String sTestCaseName;
	public String [][] testdata;
	@Parameters("sBrowser")
	@BeforeMethod
	public void starttest(String sBrowser) throws Exception{
		System.out.println( "The browser is  " +sBrowser);
		TestDriver tdDriver = new TestDriver();
		sTestCaseName = this.toString();
		
		driver = tdDriver.setuptest(sTestCaseName, sBrowser);
		testdata = tdDriver.getexceldata();
		sTestCaseName = tdDriver.sTestCaseName;
		
	}

	
	@Test
	
public void TC_Test_Login() throws Exception{	

		
		BusinessComponents bc = new BusinessComponents ();
		bc.login(driver,sTestCaseName);
		bc.searchflights(driver, sTestCaseName);
		bc.selectflight(driver);
		bc.bookflight(driver, sTestCaseName);
		bc.confirmflight(driver);
		driver.quit();
		
	}
	
	



	
}
