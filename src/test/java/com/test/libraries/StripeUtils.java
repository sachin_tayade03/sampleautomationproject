package com.test.libraries;

import com.google.gson.Gson;
import com.stripe.Stripe;
import com.test.data.LoadProperties;
import com.test.maps.CollectionOfCharges;

import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.testng.TestException;
import java.net.URI;
import java.net.URISyntaxException;
import java.io.IOException;
import java.util.Locale;

public class StripeUtils {

	private static String getAPIKey() {
		return Stripe.apiKey = LoadProperties.qaData.get("stripe.api.key").toString();
	}

	private static String getAPIVersion(){
		return Stripe.apiVersion = LoadProperties.qaData.get("stripe.api.version").toString();
	}

	private static void getAPIKey(Locale country){
		getAPIVersion();
		if (country.equals(Locale.US)){
			getAPIKey();
		}
		else {
			throw new TestException("ERROR: Stripe is used only for US. Country entered: " + country);
		}
	}

	public StripeUtils(Locale country){
		getAPIKey(country);
	}

	public static StripeUtils setAPIKey(Locale country){
		return new StripeUtils(country);
	}


	public CollectionOfCharges getListOfCharges(Integer limit){  
		CollectionOfCharges chargesRetrieved = new CollectionOfCharges();  
		Gson gson = new Gson();  

		 /* URI Builder to construct the URI we will be using to connect to the API:
	     /* Set the scheme to be https.
	     /* Set the host to be api.stripe.com. 
	     /* Set the path to be /v1/charges.
	     /* Set the parameter "limit" to be whatever limit has been passed.*/

		URIBuilder uriBuilder = new URIBuilder();  
		uriBuilder.setScheme("https")  
		.setHost("api.stripe.com")  
		.setPath("/v1/charges")  
		.setParameter("limit", limit.toString());  
		try {  
			URI uri = uriBuilder.build();  
			/* Build the URI according to the parameters we set up. 
	       Compose an HTTP GET Method based on the URI. This has to be passed to get it executed*/
			HttpGet httpGet = new HttpGet(uri);  
			/* Set up using BasicCrendentialsProvider the UsernamePasswordCredentials. 
			 * We will add the Stripe API Key here. */
			BasicCredentialsProvider credentialsProvider = new BasicCredentialsProvider();  
			credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(Stripe.apiKey, ""));  
			/* We then build with HttpClientBuilder and
			 * instantiate with CloseableHttpClient the credentials*/
			CloseableHttpClient client = HttpClientBuilder.create().setDefaultCredentialsProvider(credentialsProvider).build();  
			/*execute the Http Client using the HttpGet statement*/
			HttpResponse response = client.execute(httpGet);  
			
		// 	System.out.println(response.toString()); For checking the HTP Response
			
			/* CHECK STATUS CODE*/
			int statusCode = response.getStatusLine().getStatusCode();  
			
			
			
			if (statusCode != 200) {  
				String errorMessage = "ERROR: Attempting to Capture Charge: " + statusCode + "\n"  
						+ "Reason: " +  
						response.getStatusLine().getReasonPhrase();  
				throw new TestException (errorMessage);  
			}  
			
			
			/*Convert Results into String*/
			String json = EntityUtils.toString(response.getEntity()); 
			
			//System.out.println(json); For checking the response entity 
			/* put the response in collection of charges class using gson library*/
			chargesRetrieved = gson.fromJson(json, CollectionOfCharges.class);  
			client.close();  
		} catch (URISyntaxException e) {  
			e.printStackTrace();  
		} catch (IOException e) {  
			e.printStackTrace();  
		}  
		return chargesRetrieved;  
	}  

	public boolean checkMatchingValues(String testHeading, Object actualValue, Object expectedValue) {  
	     String successMessage = "\t* The Expected and Actual Values match. (PASS)\n";  
	     String failureMessage = "\t* The Expected and Actual Values do not match! (FAIL)\n";  
	   
	     boolean doValuesMatch = false;  
	   
	     System.out.println(testHeading);  
	     System.out.println("\t* Expected Value: " + expectedValue);  
	     System.out.println("\t* Actual Value: " + actualValue);  
	   
	     if (actualValue.equals(expectedValue)) {  
	       System.out.println(successMessage);  
	       doValuesMatch = true;  
	     } else {  
	       System.out.println(failureMessage);  
	       doValuesMatch = false;  
	     }  
	     return doValuesMatch;  
	   }  
	
}
